//
//  ViewController.swift
//  paperboarding
//
//  Created by Som Rith Prohos on 10/24/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import UIKit
import paper_onboarding

class ViewController: UIViewController , PaperOnboardingDataSource , PaperOnboardingDelegate {

    @IBOutlet weak var onBoardingView: OnboardingView!
    
    @IBOutlet weak var getStartedBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        onBoardingView.dataSource = self
        onBoardingView.delegate = self
        
    }
    
    func onboardingItemsCount() -> Int {
        return 3
    }
    
    func onboardingItemAtIndex(_ index: Int) -> OnboardingItemInfo {
        let backgroundColorOne = UIColor(red: 217/255, green: 72/255, blue: 89/255, alpha: 1)
        let backgroundColorTwo = UIColor(red: 106/255, green: 166/255, blue: 211/255, alpha: 1)
        let backgroundColorThree = UIColor(red: 168/255, green: 200/255, blue: 78/255, alpha: 1)
        
        let titleFont = UIFont(name: "AvenirNext-Bold", size: 24)!
        let descriptionFont = UIFont(name: "AvenirNext-Regular", size: 18)!
        
        
        return [("rocket","A Great Rocket Start","Dessert dessert caramels bear claw pastry. Gummi bears dragée tootsie roll gingerbread cheesecake apple pie bear claw gummies. Croissant lollipop tart candy.","",backgroundColorOne,UIColor.white,UIColor.white,titleFont,descriptionFont)
            ,("notification","Stay Up To Date","Cake brownie sesame snaps cheesecake pie marshmallow cake toffee. Bonbon jelly beans liquorice. Icing donut tiramisu powder candy toffee.","",backgroundColorTwo,UIColor.white,UIColor.white,titleFont,descriptionFont)
            ,("brush","Design Your Experience","Candy canes bonbon tootsie roll. Biscuit oat cake cheesecake sugar plum. Cake bear claw marshmallow tootsie roll jelly beans gummi bears chupa chups cake.","",backgroundColorThree,UIColor.white,UIColor.white,titleFont,descriptionFont)][index]
    }
    
   
    @objc func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
       
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        if index == 1 {
            
            if self.getStartedBtn.alpha == 1 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.getStartedBtn.alpha = 0
                })
            }
            
        }
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
        if index == 2 {
            UIView.animate(withDuration: 0.4, animations: {
                self.getStartedBtn.alpha = 1
            })
        }
    }
    
    
    @IBAction func getStartedPressed(_ sender: Any) {
        let userDefualt = UserDefaults.standard
        
        userDefualt.set(true, forKey: "onBoardingCompleted")
        
        userDefualt.synchronize()
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

  

}

